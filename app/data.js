let newItem = function (id, name, category, value) {
    this.id = id;
    this.name = name;
    this.category = category;
    this.value = +value;
};

let data = {
    allItems: {
        income: [],
        expense: []
    },
    budget: {
        income: 0,
        expense: 0,
    },
    currentBudget: 0,
};


function addItem(type, name, category, value) {
    let id, item;
    if (data.allItems[type].length > 0) {
        id = data.allItems[type][data.allItems[type].length - 1].id + 1;
    } else {
        id = 0;
    }
    item = new newItem(id, name, category, value);
    data.allItems[type].push(item);
    return item;
}

function updateBudget() {
    let income = 0,
        expense = 0;
    for (let item of data.allItems.income) {
        income = income + item.value;
    }
    for (let item of data.allItems.expense) {
        expense = expense + item.value;
    }
    data.currentBudget = income - expense;
}

function deleteItem(id, type) {
    let idNumber, curEl, curElNumber;
    idNumber = +id.split('-')[1];
    for (let i = 0; i < data.allItems[type].length; i++) {
        curEl = data.allItems[type][i];
        curElNumber = curEl.id;
        if (curElNumber === idNumber) {
            data.allItems[type].splice(i, 1);
        }
    }
}

export {addItem, updateBudget, data, deleteItem};