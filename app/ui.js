const DOM = {
    budgetItemType: '.budget__controller_type',
    budgetItemName: '.budget__controller_name',
    budgetItemCategory: '.budget__controller_category',
    budgetItemValue: '.budget__controller_value',
    budgetItemAdd: '.budget__controller_btn',
    budgetItemRemove: '.budget-table__body',
    budgetCurrent: '.budget__current',
    budgetIncome: '.budget__statistics_value--income',
    budgetExpense: '.budget__statistics_value--expense',
    select: '.select',
    selectCurrent: '.select__current',
    selectCurrentText: '.select__current_text',
    selectItem: '.select__list_item',
    tableIncome: '.budget-table__income',
    tableExpense: '.budget-table__expense'
};


function getInput() {
    return {
        type: document.querySelector(DOM.budgetItemType).querySelector(DOM.selectCurrentText).textContent.toLowerCase(),
        name: document.querySelector(DOM.budgetItemName).value,
        category: document.querySelector(DOM.budgetItemCategory + ' .is-chosen').querySelector(DOM.selectCurrentText).textContent,
        value: document.querySelector(DOM.budgetItemValue).value,
    }
}

function selectInit() {
    let current = document.querySelectorAll(DOM.selectCurrent),
        selectItem = document.querySelectorAll(DOM.selectItem),
        selectItemType = document.querySelector(DOM.budgetItemType).querySelectorAll(DOM.selectItem);
    for (let item of current) {
        item.addEventListener('click', () => {
            item.parentElement.classList.toggle('is-open');
        })
    }
    for (let item of selectItem) {
        item.addEventListener('click', selectChange);
    }
    for (let item of selectItemType) {
        item.addEventListener('click', selectType);
    }
}

function selectChange() {
    let select = this.closest(DOM.select);
    select.querySelector(DOM.selectCurrentText).textContent = this.textContent;
    select.classList.remove('is-open');
}

function selectType() {
    let select = DOM.select;
    let currentType = this.closest(select).querySelector(DOM.selectCurrentText).textContent.toLowerCase(),
        selectType = document.querySelector(DOM.budgetItemCategory).querySelectorAll(select);
    for (let item of selectType) {
        item.classList.toggle('is-chosen', item.dataset.type === currentType);
    }
}


function clearInput() {
    document.querySelector(DOM.budgetItemName).value = '';
    document.querySelector(DOM.budgetItemValue).value = '';
}

function updateBudget(income, expense, currentBudget) {
    document.querySelector(DOM.budgetCurrent).innerHTML = currentBudget + ' &#8381;';
    document.querySelector(DOM.budgetIncome).innerHTML = income + ' &#8381;';
    document.querySelector(DOM.budgetExpense).innerHTML = expense + ' &#8381;';
}

function addItem(item, type) {
    let html;
    html = `<div class="budget-table__row" id="${type}-${item.id }"><div class="budget-table__cell budget-table__cell--worth"><span class="budget-table__number">${item.value} &#8381</span></div><div class="budget-table__cell budget-table__cell--info"><span class="budget-table__name">${item.name}</span><span class="budget-table__category">${item.category}</span></div><div class="budget-table__cell budget-table__cell--remove">&times;</div>`;
    if (type === 'income') {
        document.querySelector(DOM.tableIncome).insertAdjacentHTML('beforeend', html)
    } else {
        document.querySelector(DOM.tableExpense).insertAdjacentHTML('beforeend', html)
    }
}

function deleteItem(id) {
    document.getElementById(id).remove();
}


export {DOM, getInput, selectInit, selectChange, selectType, clearInput, updateBudget, addItem, deleteItem};