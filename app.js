import * as UiCtrl from "./app/ui.js"
import * as DataCtrl from "./app/data.js"

let appController = () => {
    let DOM = UiCtrl.DOM;

    let setupEventListener = () => {
        document.querySelector(DOM.budgetItemAdd).addEventListener('click', addItem);
        document.querySelector(DOM.budgetItemRemove).addEventListener('click', deleteItem);
    };

    let addItem = () => {
        let inputData = UiCtrl.getInput(),
            newItem;
        if (inputData.name.length > 0 &&
            !isNaN(inputData.value) &&
            inputData.value > 0 &&
            inputData.category !== 'Category') {

            // 1. Add item to data
            newItem = DataCtrl.addItem(inputData.type, inputData.name, inputData.category, inputData.value);

            // 2. Add item to UI
            UiCtrl.addItem(newItem, inputData.type);

            // 3. UpdateBudget
            updateBudget();

            // 4. Clear value input
            UiCtrl.clearInput();

        }
    };

    let deleteItem = (event) => {
        let item, id, type;
        item = event.target;
        if (item.classList.contains('budget-table__cell--remove')) {
            id = item.closest('.budget-table__row').id;
            type = (item.closest('.budget-table__income')) ? 'income' : 'expense';

            DataCtrl.deleteItem(id, type);

            UiCtrl.deleteItem(id);

            updateBudget();
        }

    };

    let updateBudget = () => {

        DataCtrl.updateBudget();

        let income = DataCtrl.data.budget.income,
            expense = DataCtrl.data.budget.expense,
            currentBudget = DataCtrl.data.currentBudget;

        UiCtrl.updateBudget(income, expense, currentBudget);
    };

    return {
        init: () => {
            UiCtrl.selectInit();
            setupEventListener();
        }
    }
};

appController().init();
